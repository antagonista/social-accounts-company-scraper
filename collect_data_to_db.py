#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Collect data from csv file to database

Script used to collect data from csv file to sqlite3 database
"""

__author__ = "Maksym Kawelski"
__copyright__ = "Copyright 2020, Social accounts company scraper"
__credits__ = ["Maksym Kawelski"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Maksym Kawelski"
__status__ = "Production"

import sqlite3

# Set database connection
db_handle = sqlite3.connect('data/stock_company.sqlite3')
db_cursor = db_handle.cursor()

# Open file with company data
file_company_list = open('data/stock_company_list.csv', 'r')

for company in file_company_list:

    # Prepare data
    company = company.split('|')
    company[1] = company[1].replace('\n', '')

    if company[1] is not '':
        # Get only company with url

        db_cursor.execute('INSERT INTO `stock_company` VALUES(NULL,?,?);', (company[0], company[1]))
        db_handle.commit()

# Close file and connection with database
file_company_list.close()
db_handle.close()
