BEGIN TRANSACTION;
DROP TABLE IF EXISTS "stock_company";
CREATE TABLE IF NOT EXISTS "stock_company" (
	"id"	INTEGER NOT NULL UNIQUE,
	"name"	TEXT NOT NULL,
	"url"	TEXT NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
DROP TABLE IF EXISTS "social_account";
CREATE TABLE IF NOT EXISTS "social_account" (
	"id"	INTEGER NOT NULL UNIQUE,
	"name"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT)
);
DROP TABLE IF EXISTS "stock_social_account";
CREATE TABLE IF NOT EXISTS "stock_social_account" (
	"id"	INTEGER NOT NULL UNIQUE,
	"id_stock"	INTEGER,
	"id_social_account"	INTEGER,
	"name"	TEXT,
	FOREIGN KEY("id_social_account") REFERENCES "social_account"("id"),
	FOREIGN KEY("id_stock") REFERENCES "stock_company"("id"),
	PRIMARY KEY("id" AUTOINCREMENT)
);
DROP TABLE IF EXISTS "error";
CREATE TABLE IF NOT EXISTS "error" (
	"id"	INTEGER NOT NULL UNIQUE,
	"id_stock"	INTEGER,
	"date"	TEXT,
	FOREIGN KEY("id_stock") REFERENCES "stock_company"("id"),
	PRIMARY KEY("id" AUTOINCREMENT)
);
INSERT INTO "social_account" ("id","name") VALUES (1,'twitter'),
 (2,'facebook'),
 (3,'linkedin'),
 (4,'instagram');
INSERT INTO "stock_social_account" ("id","id_stock","id_social_account","name") VALUES (23,9219,2,'integumen'),
 (24,9219,3,'integumen-limited');
COMMIT;
