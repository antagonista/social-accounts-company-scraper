# Social accounts company scraper

Simple script to scraping social accounts name from websites company list.

Script use Python (3) with Selenium (Firefox: gecodriver) and Sqlite3 to collect data.

Script get only info about social accounts from home / main website company.

## Files

* `company_sa_scraper.py` - main script to scrape
* `collect_data_do_db.py`- script add company info from csv file to sqlite3 database
* `data/stock_company.sqlite3` - database (with company data from csv file)
* `data/empty_stock_company.sqlite3` - database (without company data)
* `data/stock_company_list.csv` - company data (format: name_company|url)
* `data/dump_empty_stock_company.sqlite3.sql` - empty dump (structure only) database
* `data/dump_stock_company.sqlite3.sql`- dump with company data from csv file

## Database

### Structure
![SQL Structure Database](data/sql_structure_diagram.png)

### Description

#### Table: stock_company
Table with company data

Columns:
* id
* name - company name
* url - company website url

#### Table: error
Table with error get site company

Columns: 
* id
* id_stock - foreign key to id (stock) in table stock_company
* date - error date

#### Table: stock_social_account
Table with social account company data

Columns:
* id
* id_stock - foreign key to id (stock) in table stock_company
* id_social_account - foreign key to id in table social_account
* name - scraped name from url social account

#### Table: social_account
Dictionary table with social account names

Columns:
* id
* name - social account name