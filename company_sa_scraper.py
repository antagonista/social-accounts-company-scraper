#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Company social accounts scraper

Script used to scrap social accounts name from company web pages
"""

__author__ = "Maksym Kawelski"
__copyright__ = "Copyright 2020, Social accounts company scraper"
__credits__ = ["Maksym Kawelski"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Maksym Kawelski"
__status__ = "Production"

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from datetime import datetime
import sqlite3
import re
import pprint

# Methods


def add_social_account_to_db(db_conn, db_cur, social_account, id_company, id_social_account):
    # Methods check array with social accounts name and add to database
    # db_conn - sqlite database handle connection
    # db_cur - sqlite database handle cursor
    # social_account - array with found social accounts name
    # id_company - id company from database table `stock_company`
    # id_social_account - id social account from database table `social_account`

    if social_account:
        # Not empty list

        for account in social_account:
            # Check if account name already exists in database
            result = db_cur.execute(
                'SELECT id FROM stock_social_account WHERE id_stock = ? AND id_social_account = ? AND name = ?',
                (id_company, id_social_account, account)
            ).fetchall()

            if len(result) == 0:
                # Not exist - add to database
                db_cur.execute('INSERT INTO stock_social_account VALUES (NULL,?,?,?)',
                               (id_company, id_social_account, account))
                db_conn.commit()


# Config
startTime = datetime.now()

file_sqlite = 'data/stock_company.sqlite3'

sa_facebook = []
sa_twitter = []
sa_instagram = []
sa_linkedin = []

# Set database connection
db_conn = sqlite3.connect(file_sqlite)
db_cur = db_conn.cursor()

# Get all company data from database
db_cur.execute('SELECT * FROM stock_company')

company_list = db_cur.fetchall()
company_list = set(company_list)

# Set selenium browser
options = Options()
options.headless = True
browser = webdriver.Firefox(options=options)

# Start scraping
for company in company_list:

    try:
        # Get data from company website url
        browser.get('http://' + company[2])

        # Get all url's from site
        all_urls = browser.find_elements_by_xpath("//a[@href]")
        # Only unique url set
        all_urls = set(all_urls)

        # Search social accounts url
        for url in all_urls:

            url = url.get_attribute("href")

            # Twitter account
            found = re.search(r'twitter.com/([A-z0-9_]+)', url)

            if found:
                sa_twitter.append(found.group(1))
                continue

            # Facebook account
            found = re.search(r'facebook.com/([A-z0-9_\-.]+)', url)

            if found:
                sa_facebook.append(found.group(1))
                continue

            # Instagram account
            found = re.search('instagram.com/([A-Za-z0-9_]+)', url)

            if found:
                sa_instagram.append(found.group(1))
                continue

            # Linkedin account
            found = re.search('linkedin.com/company/([A-z0-9_-]+)', url)

            if found:
                sa_linkedin.append(found.group(1))
                continue

        # Add found social accounts to database
        add_social_account_to_db(db_conn, db_cur, sa_twitter, company[0], 1)
        add_social_account_to_db(db_conn, db_cur, sa_facebook, company[0], 2)
        add_social_account_to_db(db_conn, db_cur, sa_linkedin, company[0], 3)
        add_social_account_to_db(db_conn, db_cur, sa_instagram, company[0], 4)

    except:
        # error : 1 - Get site source code error
        db_cur.execute('INSERT INTO error VALUES (NULL, ?, CURRENT_TIMESTAMP)', (company[0],))
        db_conn.commit()

    # Clear temp arrays
    sa_facebook = []
    sa_twitter = []
    sa_instagram = []
    sa_linkedin = []

# End scraping - close browser and db connection
browser.close()
db_conn.close()

# Show time of execution script
print('- - - - - -')
endTime = datetime.now()
workAppTime = endTime - startTime
print('Time execute app: ' + workAppTime.__str__())
